# PF2e Ability Score Manager

An easy way to build a stat array for the Pathfinder 2e system
Click the edit button next to ability scores in your character sheet to access it.

### Manual Install

In Foundry setup, click on the Install Module button and put the following path in the Manifest URL.

`https://gitlab.com/pearcebasmanm/pf2e-abs-manager/-/raw/main/module.json`
